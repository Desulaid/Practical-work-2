﻿# Проект практической работы

![Главное окно](/Screens/Главное%20окно.PNG) 
#### О программе
Вот выйдет первый релиз, тогда и напишу...

#### Скриншоты
![Добавление нового придмета](/Screens/Добавление%20нового%20предмета.PNG) 
![Изменить предметы](/Screens/Изменить%20предметы.PNG) 
![О программе](/Screens/О%20программе.PNG) 

#### Если вы хотите посмотреть на мой код, а не на сгенерированный код VS, то

* [About.cs](/Practical%20work/About.cs "О программе") 
* [AddLesson.cs](/Practical%20work/AddLesson.cs "Добавить новый предмет в таблицу") 
* [EditTableStruct.cs](/Practical%20work/EditTableStruct.cs "Редактор таблицы") 
* [MainWindow.cs](/Practical%20work/MainWindow.cs "Главное окно программы")
* [DataDialog.cs](/Practical%20work/DataDialog.cs "Класс для диалогов")
* [DataStudent.cs](/Practical%20work/DataStudent.cs "Главное для студентов")
* [DataTable.cs](/Practical%20work/DataTable.cs "Класс для работы с таблицами")

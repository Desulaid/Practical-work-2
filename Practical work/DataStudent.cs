﻿/* DataStudent.cs
 * Методы для работы с данными студентов в таблице DataGridView.
 */

using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Practical_work
{
    class DataStudent
    {
        public void AddStudent(DataGridView d, string value)
        {
            List<string> row = new List<string>();

            int r_count = d.RowCount;

            if (r_count == 0)
            {
                row.Add((r_count + 1).ToString());
            }
            else
            {
                row.Add((Convert.ToInt32(d["id", r_count - 1].Value) + 1).ToString());
            }

            for (int i = 0; i < d.ColumnCount; i++)
            {
                row.Add(value);
            }

            d.Rows.Add(row.ToArray());
        }
    }
}

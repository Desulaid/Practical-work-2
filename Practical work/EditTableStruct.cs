﻿/* EditTableStruct.cs
 * Редактор пунктов таблицы.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Practical_work
{
    public partial class EditTableStruct : Form
    {
        public EditTableStruct()
        {
            InitializeComponent();
        }

        private void EditTableStruct_FormClosing(object sender, FormClosingEventArgs e)
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();
            Hide();
        }

        private void EditTableStruct_Load(object sender, EventArgs e)
        {

            infoText.Text = "Чтобы изменить или удалить столбец в таблице\n" +
                            "нажмите в вверхнем списке на пункт меню\n" +
                            "два раза.";

            { // Загрузка структуры таблицы
                string path = @".\Настройки\" + "Структура_таблицы.template";

                if (File.Exists(path))
                {
                    string[] str = File.ReadAllLines(path);
                    
                    foreach (string s in str)
                    {
                        listColumnNames.Items.Add(s.Substring(0, s.IndexOf('|')));
                    }
                }
            }

        }

        private void listColumnNames_DoubleClick(object sender, EventArgs e)
        {
            boxInputName.Text = listColumnNames.SelectedItem.ToString();
            infoText.Text = "Теперь введите новое имя столбца.\n" +
                            "Чтобы сохранить новое имя, нажмите на\n" +
                            "кнопку \"Сохранить\"\n" +
                            "Чтобы удалить пункт меню - \"Удалить\".";
        }

        private void btnEditColumnName_Click(object sender, EventArgs e)
        {
            var item = listColumnNames.SelectedIndex;
            
            listColumnNames.Items[item] = boxInputName.Text;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var item = listColumnNames.SelectedItem;
            
            listColumnNames.Items.Remove(item);
        }

        private void btnSaveAll_Click(object sender, EventArgs e)
        {
            List<string> items = new List<string>();

            string item_name;

            for (int i = 0; i < listColumnNames.Items.Count; i++)
            {
                item_name = listColumnNames.Items[i].ToString();
                items.Add(item_name + "|dev_" + item_name);
            }

            string path = @".\Настройки\Структура_таблицы.template";
            File.WriteAllLines(path, items.ToArray());

            var mainWindow = new MainWindow();
            mainWindow.Show();
            Hide();
        }
    }
}

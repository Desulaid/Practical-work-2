﻿namespace Practical_work
{
    partial class MainWindow
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.Table = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birth_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.start_edu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dock_edu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.graduate_work_th = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.course_work = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subMenu = new System.Windows.Forms.MenuStrip();
            this.menuAddStudent = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDeleteStudent = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAddLesson = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEditStruct = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.mainMenuApp = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mainMenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuOpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuSaveFile = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.Table)).BeginInit();
            this.subMenu.SuspendLayout();
            this.mainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // Table
            // 
            this.Table.AllowUserToAddRows = false;
            this.Table.AllowUserToDeleteRows = false;
            this.Table.AllowUserToOrderColumns = true;
            this.Table.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.Table.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.name,
            this.birth_date,
            this.start_edu,
            this.dock_edu,
            this.graduate_work_th,
            this.course_work});
            this.Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Table.GridColor = System.Drawing.SystemColors.Control;
            this.Table.Location = new System.Drawing.Point(0, 47);
            this.Table.Name = "Table";
            this.Table.Size = new System.Drawing.Size(1031, 652);
            this.Table.TabIndex = 1;
            // 
            // id
            // 
            this.id.HeaderText = "№ п/п";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // name
            // 
            this.name.HeaderText = "Фамилия, имя, отчество";
            this.name.Name = "name";
            // 
            // birth_date
            // 
            this.birth_date.HeaderText = "Дата рождения";
            this.birth_date.Name = "birth_date";
            // 
            // start_edu
            // 
            this.start_edu.HeaderText = "Год поступления";
            this.start_edu.Name = "start_edu";
            // 
            // dock_edu
            // 
            this.dock_edu.HeaderText = "Документ об образовании";
            this.dock_edu.Name = "dock_edu";
            // 
            // graduate_work_th
            // 
            this.graduate_work_th.HeaderText = "Тема дипломной работы";
            this.graduate_work_th.Name = "graduate_work_th";
            // 
            // course_work
            // 
            this.course_work.FillWeight = 200F;
            this.course_work.HeaderText = "Курсовая работа (дисциплина, оценка за защиту)";
            this.course_work.Name = "course_work";
            this.course_work.Width = 200;
            // 
            // subMenu
            // 
            this.subMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuAddStudent,
            this.menuDeleteStudent,
            this.menuAddLesson,
            this.menuEditStruct});
            this.subMenu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.subMenu.Location = new System.Drawing.Point(0, 24);
            this.subMenu.Name = "subMenu";
            this.subMenu.Size = new System.Drawing.Size(1031, 23);
            this.subMenu.TabIndex = 6;
            this.subMenu.Text = "menuStrip1";
            // 
            // menuAddStudent
            // 
            this.menuAddStudent.Name = "menuAddStudent";
            this.menuAddStudent.Size = new System.Drawing.Size(121, 19);
            this.menuAddStudent.Text = "Добавить студента";
            this.menuAddStudent.Click += new System.EventHandler(this.menuAddStudent_Click);
            // 
            // menuDeleteStudent
            // 
            this.menuDeleteStudent.Name = "menuDeleteStudent";
            this.menuDeleteStudent.Size = new System.Drawing.Size(113, 19);
            this.menuDeleteStudent.Text = "Удалить студента";
            this.menuDeleteStudent.Click += new System.EventHandler(this.menuDeleteStudent_Click);
            // 
            // menuAddLesson
            // 
            this.menuAddLesson.Name = "menuAddLesson";
            this.menuAddLesson.Size = new System.Drawing.Size(120, 19);
            this.menuAddLesson.Text = "Добавить предмет";
            this.menuAddLesson.Click += new System.EventHandler(this.menuAddLesson_Click);
            // 
            // menuEditStruct
            // 
            this.menuEditStruct.Name = "menuEditStruct";
            this.menuEditStruct.Size = new System.Drawing.Size(131, 19);
            this.menuEditStruct.Text = "Изменить предметы";
            this.menuEditStruct.Click += new System.EventHandler(this.menuEditStruct_Click);
            // 
            // mainMenu
            // 
            this.mainMenu.BackColor = System.Drawing.SystemColors.MenuBar;
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuApp,
            this.mainMenuExcel});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(1031, 24);
            this.mainMenu.TabIndex = 7;
            this.mainMenu.Text = "menuStrip1";
            // 
            // mainMenuApp
            // 
            this.mainMenuApp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuInfo,
            this.mainMenuAbout,
            this.toolStripSeparator1,
            this.mainMenuExit});
            this.mainMenuApp.Name = "mainMenuApp";
            this.mainMenuApp.Size = new System.Drawing.Size(84, 20);
            this.mainMenuApp.Text = "Программа";
            // 
            // mainMenuInfo
            // 
            this.mainMenuInfo.Name = "mainMenuInfo";
            this.mainMenuInfo.Size = new System.Drawing.Size(152, 22);
            this.mainMenuInfo.Text = "Справка";
            // 
            // mainMenuAbout
            // 
            this.mainMenuAbout.Name = "mainMenuAbout";
            this.mainMenuAbout.Size = new System.Drawing.Size(152, 22);
            this.mainMenuAbout.Text = "О программе";
            this.mainMenuAbout.Click += new System.EventHandler(this.mainMenuAbout_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // mainMenuExit
            // 
            this.mainMenuExit.Name = "mainMenuExit";
            this.mainMenuExit.Size = new System.Drawing.Size(152, 22);
            this.mainMenuExit.Text = "Выход";
            this.mainMenuExit.Click += new System.EventHandler(this.mainMenuExit_Click);
            // 
            // mainMenuExcel
            // 
            this.mainMenuExcel.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainMenuOpenFile,
            this.mainMenuSaveFile});
            this.mainMenuExcel.Name = "mainMenuExcel";
            this.mainMenuExcel.Size = new System.Drawing.Size(65, 20);
            this.mainMenuExcel.Text = "MS Excel";
            // 
            // mainMenuOpenFile
            // 
            this.mainMenuOpenFile.Name = "mainMenuOpenFile";
            this.mainMenuOpenFile.Size = new System.Drawing.Size(164, 22);
            this.mainMenuOpenFile.Text = "Открыть файл";
            this.mainMenuOpenFile.Click += new System.EventHandler(this.mainMenuOpenFile_Click);
            // 
            // mainMenuSaveFile
            // 
            this.mainMenuSaveFile.Name = "mainMenuSaveFile";
            this.mainMenuSaveFile.Size = new System.Drawing.Size(164, 22);
            this.mainMenuSaveFile.Text = "Сохранить файл";
            this.mainMenuSaveFile.Click += new System.EventHandler(this.mainMenuSaveFile_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "Открыть ведомость";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 699);
            this.Controls.Add(this.Table);
            this.Controls.Add(this.subMenu);
            this.Controls.Add(this.mainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.subMenu;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ведомость.NET";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Table)).EndInit();
            this.subMenu.ResumeLayout(false);
            this.subMenu.PerformLayout();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.DataGridView Table;
        public System.Windows.Forms.DataGridViewTextBoxColumn id;
        public System.Windows.Forms.DataGridViewTextBoxColumn name;
        public System.Windows.Forms.DataGridViewTextBoxColumn birth_date;
        public System.Windows.Forms.DataGridViewTextBoxColumn start_edu;
        public System.Windows.Forms.DataGridViewTextBoxColumn dock_edu;
        public System.Windows.Forms.DataGridViewTextBoxColumn graduate_work_th;
        public System.Windows.Forms.DataGridViewTextBoxColumn course_work;
        private System.Windows.Forms.MenuStrip subMenu;
        private System.Windows.Forms.ToolStripMenuItem menuAddStudent;
        private System.Windows.Forms.ToolStripMenuItem menuDeleteStudent;
        private System.Windows.Forms.ToolStripMenuItem menuAddLesson;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem mainMenuApp;
        private System.Windows.Forms.ToolStripMenuItem mainMenuInfo;
        private System.Windows.Forms.ToolStripMenuItem mainMenuAbout;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mainMenuExit;
        private System.Windows.Forms.ToolStripMenuItem menuEditStruct;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.ToolStripMenuItem mainMenuExcel;
        private System.Windows.Forms.ToolStripMenuItem mainMenuOpenFile;
        private System.Windows.Forms.ToolStripMenuItem mainMenuSaveFile;
    }
}


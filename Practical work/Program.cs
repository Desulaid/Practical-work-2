﻿/* Program.cs
 * Главная точка входа для приложения.
 */

using System;
using System.Windows.Forms;

namespace Practical_work
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainWindow());
        }
    }
}

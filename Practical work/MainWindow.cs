﻿/* MainWindow.cs
 * Стартовое окно приложения.
 */

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;

namespace Practical_work
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /* Чтобы не писать много раз одно и то же, а то лень... :smile:
         */

        private void LoadTableStruct(string path, string name)
        {
            var dataTable = new DataTable();
            dataTable.LoadStruct(Table, path + name);
        }

        /* События главного окна.
         */

        private void MainWindow_Load(object sender, EventArgs e)
        {
            string path = @".\Настройки\";

            LoadTableStruct(path, "Структура_таблицы.template");

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(
                    "Вы действительно хотите выйти?\n" +
                    "Если вы выйдите несохранившись, " +
                    "то ваши данные будут потеряны безвовзратно.",
                    "Выход",
                MessageBoxButtons.OKCancel) == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
        }

        private void menuAddStudent_Click(object sender, EventArgs e)
        {
            var dataTable = new DataTable();
            dataTable.AddStudent(Table, "-");

            menuAddLesson.Enabled = 
            menuEditStruct.Enabled = false;
        }

        private void menuAddLesson_Click(object sender, EventArgs e)
        {
            //SaveTableData(@"\", "Ведомость.xls");
            
            var addLesson = new AddLesson();
            addLesson.Show();
            Hide();
        }

        private void menuDeleteStudent_Click(object sender, EventArgs e)
        {
            int count = Table.RowCount;

            if (count - 1 == 0)
            {
                menuAddLesson.Enabled =
                menuEditStruct.Enabled = true;
            }

            if (count > 0)
                Table.Rows.RemoveAt(Table.SelectedCells[0].RowIndex);
            else
                dataDialog.ShowMessage("Информация", "Нет данных для удаления!");
        }

        private void mainMenuAbout_Click(object sender, EventArgs e)
        {
            var about = new About();
            about.Show();
        }

        private void mainMenuExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void menuEditStruct_Click(object sender, EventArgs e)
        {
            var editTableStruct = new EditTableStruct();
            editTableStruct.Show();
            Hide();
        }

        private void mainMenuOpenFile_Click(object sender, EventArgs e)
        {
            openFileDialog.Title = "Открыть ведомость";
            openFileDialog.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            openFileDialog.Filter = "Excel Worksheets|*.xls";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string path = openFileDialog.FileName;

                var xl = new Excel.Application();

                xl.Visible = false;

                var book = xl.Workbooks.Open(path);

                Excel.Worksheet sheets = book.Worksheets.get_Item(1);

                var rangle = sheets.UsedRange;

                List<string> row = new List<string>();

                Table.Rows.Add(rangle.Rows.Count - 1);

                for (int i = 1; i <= rangle.Columns.Count; i++)
                {
                    try
                    {
                        Table.Columns[i - 1].HeaderText = xl.Cells[1, i].Value;
                    }
                    catch (ArgumentOutOfRangeException)
                    { 
                        Table.Columns.Add(new DataGridViewTextBoxColumn()
                        {
                            Name = "dev_" + xl.Cells[1, i].Value,
                            HeaderText = xl.Cells[1, i].Value
                        });
                    }
                    
                    for (int j = 2; j <= rangle.Rows.Count; j++)
                    {
                        Table[i - 1, j - 2].Value = Convert.ToString(rangle.Cells[j, i].Value);
                    }
                }
                book.Close(true);
                xl.Quit();
            }
        }

        private void mainMenuSaveFile_Click(object sender, EventArgs e)
        {
            saveFileDialog.Title = "Сохранить ведомость";
            saveFileDialog.InitialDirectory = System.IO.Directory.GetCurrentDirectory();
            saveFileDialog.Filter = "Excel Worksheets|*.xls";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (saveFileDialog.FileName != null)
                {
                    string path = saveFileDialog.FileName;

                    const int START_POS = 2;

                    var xl = new Excel.Application();

                    xl.Visible = false;

                    var book = xl.Workbooks.Add();

                    Excel.Worksheet sheets = (Excel.Worksheet)xl.ActiveSheet;

                    int c_count = Table.ColumnCount;

                    for (int i = 0; i < c_count; i++)
                    {
                        sheets.Cells[1, i + 1] = Table.Columns[i].HeaderText;

                        for (int j = 0; j < Table.RowCount; j++)
                            sheets.Cells[START_POS + j, i + 1] = (Table[i, j].Value).ToString();
                    }

                    book.SaveCopyAs(path);
                    book.Saved = true;
                    book.Close(true);
                    xl.Quit();
                }
            }
        }
    }
}

﻿/* DataTablse.cs
 * Методы для работы с данными таблицы DataGridView.
 */

using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;

namespace Practical_work
{
    class DataTable
    {
        public void LoadStruct(DataGridView d, string path)
        {
            if (File.Exists(path))
            {
                string name, h_text;
                string[] str = File.ReadAllLines(path);

                foreach (string s in str)
                {
                    h_text = s.Substring(0, s.IndexOf('|'));
                    name = s.Substring(s.IndexOf('|') + 1);

                    d.Columns.Add(new DataGridViewTextBoxColumn()
                    {
                        Name = name,
                        HeaderText = h_text
                    });
                }
            }
        }
        
        public void AddStudent(DataGridView d, string value)
        {
            List<string> row = new List<string>();

            int r_count = d.RowCount;

            if (r_count == 0)
                row.Add((r_count + 1).ToString());
            else
                row.Add((System.Convert.ToInt32(d["id", r_count - 1].Value) + 1).ToString());

            for (int i = 0; i < d.ColumnCount; i++)
                row.Add(value);
            d.Rows.Add(row.ToArray());
        }
    }
}

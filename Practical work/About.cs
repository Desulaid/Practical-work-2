﻿/* About.cs
 * Окно "О программе".
 */

using System;
using System.Windows.Forms;

namespace Practical_work
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        private void About_Load(object sender, EventArgs e)
        {
            Info.Text = String.Format(
                "Приложение для автоматизации работы с ведомостями{1}{1}"+
                "Версия приложения: {0}{1}Наименование: {2}{1}{1}© 2018, Стяжкин Антон", 
                
                Application.ProductVersion, 
                Environment.NewLine, 
                Application.ProductName
            );
            Info.Enabled = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

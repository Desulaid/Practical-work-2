﻿namespace Practical_work
{
    partial class AddLesson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.boxLessonName = new System.Windows.Forms.TextBox();
            this.btnAddLessonInTable = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // boxLessonName
            // 
            this.boxLessonName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.boxLessonName.Location = new System.Drawing.Point(11, 12);
            this.boxLessonName.Name = "boxLessonName";
            this.boxLessonName.Size = new System.Drawing.Size(401, 24);
            this.boxLessonName.TabIndex = 0;
            this.boxLessonName.Text = "Введите название предмета";
            this.boxLessonName.Click += new System.EventHandler(this.boxLessonName_Click);
            // 
            // btnAddLessonInTable
            // 
            this.btnAddLessonInTable.Location = new System.Drawing.Point(12, 51);
            this.btnAddLessonInTable.Name = "btnAddLessonInTable";
            this.btnAddLessonInTable.Size = new System.Drawing.Size(400, 26);
            this.btnAddLessonInTable.TabIndex = 1;
            this.btnAddLessonInTable.Text = "Добавить";
            this.btnAddLessonInTable.UseVisualStyleBackColor = true;
            this.btnAddLessonInTable.Click += new System.EventHandler(this.btnAddLessonInTable_Click);
            // 
            // AddLesson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 89);
            this.Controls.Add(this.btnAddLessonInTable);
            this.Controls.Add(this.boxLessonName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "AddLesson";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Добавление нового предмета";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AddLesson_FormClosing);
            this.Click += new System.EventHandler(this.AddLesson_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox boxLessonName;
        private System.Windows.Forms.Button btnAddLessonInTable;
    }
}
﻿/* DataDialog.cs
 * Чтобы не дублировать лишний код.
 */

using System.Media;
using System.Windows.Forms;

namespace Practical_work
{
    class dataDialog
    {
        public static DialogResult ShowMessage(string caption, string message)
        {
            SystemSounds.Beep.Play();
            MessageBox.Show(message, caption);

            return DialogResult.OK;
        }
    }
}

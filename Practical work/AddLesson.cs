﻿/* Addlesson.cs
 * Форма для добавления новых столбцов в таблицу DataGridView.
 */

using System;
using System.IO;
using System.Windows.Forms;

namespace Practical_work
{
    public partial class AddLesson : Form
    {
        public AddLesson()
        {
            InitializeComponent();
        }

        /* Чтобы не писать много раз одно и то же, а то лень... :smile: X2
         */

        private void ShowMainWindow()
        {
            var mainWindow = new MainWindow();
            mainWindow.Show();

            Hide();
        }

        /* События окна
         */

        private void AddLesson_Click(object sender, EventArgs e)
        {
            boxLessonName.Text = "Введите название предмета";
        }

        private void boxLessonName_Click(object sender, EventArgs e)
        {
            boxLessonName.Text = null;
        }

        private void btnAddLessonInTable_Click(object sender, EventArgs e)
        {
            File.AppendAllText(@".\Настройки\Структура_таблицы.template", boxLessonName.Text + "|dev_" + boxLessonName.Text + "\r\n");
            ShowMainWindow();
        }

        private void AddLesson_FormClosing(object sender, FormClosingEventArgs e)
        {
            ShowMainWindow();
        }
    }
}
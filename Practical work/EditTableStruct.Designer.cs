﻿namespace Practical_work
{
    partial class EditTableStruct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listColumnNames = new System.Windows.Forms.ListBox();
            this.boxInputName = new System.Windows.Forms.TextBox();
            this.btnEditColumnName = new System.Windows.Forms.Button();
            this.btnSaveAll = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.infoText = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // listColumnNames
            // 
            this.listColumnNames.FormattingEnabled = true;
            this.listColumnNames.Location = new System.Drawing.Point(6, 19);
            this.listColumnNames.Name = "listColumnNames";
            this.listColumnNames.Size = new System.Drawing.Size(253, 238);
            this.listColumnNames.TabIndex = 0;
            this.listColumnNames.DoubleClick += new System.EventHandler(this.listColumnNames_DoubleClick);
            // 
            // boxInputName
            // 
            this.boxInputName.Location = new System.Drawing.Point(6, 19);
            this.boxInputName.Name = "boxInputName";
            this.boxInputName.Size = new System.Drawing.Size(253, 20);
            this.boxInputName.TabIndex = 1;
            // 
            // btnEditColumnName
            // 
            this.btnEditColumnName.Location = new System.Drawing.Point(6, 19);
            this.btnEditColumnName.Name = "btnEditColumnName";
            this.btnEditColumnName.Size = new System.Drawing.Size(78, 31);
            this.btnEditColumnName.TabIndex = 3;
            this.btnEditColumnName.Text = "Изменить";
            this.btnEditColumnName.UseVisualStyleBackColor = true;
            this.btnEditColumnName.Click += new System.EventHandler(this.btnEditColumnName_Click);
            // 
            // btnSaveAll
            // 
            this.btnSaveAll.Location = new System.Drawing.Point(182, 19);
            this.btnSaveAll.Name = "btnSaveAll";
            this.btnSaveAll.Size = new System.Drawing.Size(77, 31);
            this.btnSaveAll.TabIndex = 4;
            this.btnSaveAll.Text = "Сохранить";
            this.btnSaveAll.UseVisualStyleBackColor = true;
            this.btnSaveAll.Click += new System.EventHandler(this.btnSaveAll_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.infoText);
            this.groupBox1.Location = new System.Drawing.Point(283, 66);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(265, 139);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Подсказка:";
            // 
            // infoText
            // 
            this.infoText.AutoSize = true;
            this.infoText.Location = new System.Drawing.Point(7, 32);
            this.infoText.Name = "infoText";
            this.infoText.Size = new System.Drawing.Size(35, 13);
            this.infoText.TabIndex = 0;
            this.infoText.Text = "label1";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(90, 19);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(86, 31);
            this.btnDelete.TabIndex = 6;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listColumnNames);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(265, 264);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Доступные пункты в меню";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.boxInputName);
            this.groupBox3.Location = new System.Drawing.Point(283, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(265, 48);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Название колонки:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnEditColumnName);
            this.groupBox4.Controls.Add(this.btnSaveAll);
            this.groupBox4.Controls.Add(this.btnDelete);
            this.groupBox4.Location = new System.Drawing.Point(283, 211);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(265, 65);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Доступные действия";
            // 
            // EditTableStruct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 286);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "EditTableStruct";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Изменить предметы";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditTableStruct_FormClosing);
            this.Load += new System.EventHandler(this.EditTableStruct_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listColumnNames;
        private System.Windows.Forms.TextBox boxInputName;
        private System.Windows.Forms.Button btnEditColumnName;
        private System.Windows.Forms.Button btnSaveAll;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label infoText;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}